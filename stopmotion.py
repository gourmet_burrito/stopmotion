import cv2
import sys
from PyQt5.QtWidgets import QWidget, QLabel, QSlider, QApplication, QVBoxLayout, QHBoxLayout, QSizePolicy, QLineEdit, \
    QSpinBox, QScrollArea
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot, QSize
from PyQt5.QtGui import QImage, QPixmap

cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)

cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)


# 0. CV_CAP_PROP_BRIGHTNESS Brightness of the image (only for cameras).
# 11. CV_CAP_PROP_CONTRAST Contrast of the image (only for cameras).
# 12. CV_CAP_PROP_SATURATION Saturation of the image (only for cameras).
# 13. CV_CAP_PROP_HUE Hue of the image (only for cameras).
# 14. CV_CAP_PROP_GAIN Gain of the image (only for cameras).
# 15. CV_CAP_PROP_EXPOSURE Exposure (only for cameras).
# 16. CV_CAP_PROP_CONVERT_RGB Boolean flags indicating whether images should be converted to RGB.
# 17. CV_CAP_PROP_WHITE_BALANCE Currently unsupported


# print(cap.get(cv2.CAP_PROP_FOCUS))
# print(cap.get(cv2.CAP_PROP_EXPOSURE))
# print(cap.get(cv2.CAP_PROP_BRIGHTNESS))
# print(cap.get(cv2.CAP_PROP_CONTRAST))
# print(cv2.CAP_PROP_SATURATION)
# print(cap.get(cv2.CAP_PROP_HUE))
# print(cap.get(cv2.CAP_PROP_GAIN))

def setFocus(focus):
    cap.set(cv2.CAP_PROP_FOCUS, focus)


def setExposure(exposure):
    cap.set(cv2.CAP_PROP_EXPOSURE, exposure)


def setCVParam(param, value):
    cap.set(param, value)


class Thread(QThread):
    changePixmap = pyqtSignal(QImage)
    pause = False

    def run(self):
        while True:
            if self.pause:
                continue

            ret, frame = cap.read()
            if ret:
                # https://stackoverflow.com/a/55468544/6622587
                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h, bytesPerLine, QImage.Format_RGB888)
                p = convertToQtFormat.scaled(1920, 1080, Qt.KeepAspectRatio)
                self.changePixmap.emit(p)


class LabeledSlider(QWidget):
    def __init__(self, label, minimum, maximum):
        super().__init__()
        self.slider = QSlider(Qt.Horizontal, self)
        self.slider.setMinimum(minimum)
        self.slider.setMaximum(maximum)
        self.slider.setTickInterval(1)
        self.slider.setSingleStep(1)
        self.slider.setTickPosition(QSlider.TicksBelow)

        self.label = QLabel(label, self)
        self.label.setFixedWidth(75)
        self.value_display = QLineEdit('', self)
        self.value_display.setMaximumWidth(50)

        layout = QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(5)
        self.setLayout(layout)

        layout.addWidget(self.label)
        layout.addWidget(self.slider)
        layout.addWidget(self.value_display)

        self.slider.valueChanged.connect(self.sliderChanged)

    def sliderChanged(self):
        self.value_display.setText(str(self.slider.value()))


class OutputPath(QWidget):
    def __init__(self):
        super().__init__()
        self.label = QLabel('Output Path:')
        self.path = QLineEdit('d:/frame.{frame:04d}.png', self)
        self.frame = QSpinBox(self)
        self.frame.setValue(0)
        self.frame.setMinimum(0)

        layout = QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(5)
        self.setLayout(layout)

        layout.addWidget(self.label)
        layout.addWidget(self.path)
        layout.addWidget(self.frame)

    def getOutputPath(self):
        return self.path.text()

    def getFrame(self):
        return self.frame.value()

    def setFrame(self, value):
        self.frame.setValue(value)


class Controls(QWidget):
    def __init__(self):
        super().__init__()

        self.focus = LabeledSlider('Focus', 0, 51)
        self.exposure = LabeledSlider('Exposure', -13, -1)
        self.brightness = LabeledSlider('Brightness', 0, 255)
        self.contrast = LabeledSlider('Contrast', 0, 255)
        self.saturation = LabeledSlider('Saturation', 0, 255)
        # self.hue = LabeledSlider('Hue', -10, 10)
        # self.gain = LabeledSlider('Gain', 0, 255)

        self.outputPath = OutputPath()

        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)

        layout.addWidget(self.focus)
        layout.addWidget(self.exposure)
        layout.addWidget(self.brightness)
        layout.addWidget(self.contrast)
        layout.addWidget(self.saturation)
        # layout.addWidget(self.hue)
        # layout.addWidget(self.gain)
        layout.addStretch()
        layout.addWidget(self.outputPath)

        self.focus.slider.valueChanged.connect(self.setFocusSlot)
        self.exposure.slider.valueChanged.connect(self.setExposureSlot)
        self.brightness.slider.valueChanged.connect(self.setBrightnessSlot)
        self.contrast.slider.valueChanged.connect(self.setContrastSlot)
        self.saturation.slider.valueChanged.connect(self.setSaturationSlot)
        # self.hue.slider.valueChanged.connect(self.setHueSlot)
        # self.gain.slider.valueChanged.connect(self.setGainSlot)

        self.focus.slider.setValue(cap.get(cv2.CAP_PROP_FOCUS))
        self.exposure.slider.setValue(cap.get(cv2.CAP_PROP_EXPOSURE))
        self.brightness.slider.setValue(cap.get(cv2.CAP_PROP_BRIGHTNESS))
        self.contrast.slider.setValue(cap.get(cv2.CAP_PROP_CONTRAST))
        self.saturation.slider.setValue(cv2.CAP_PROP_SATURATION)
        self.saturation.slider.setValue(100)
        # self.hue.slider.setValue(cap.get(cv2.CAP_PROP_HUE))
        # self.gain.slider.setValue(cap.get(cv2.CAP_PROP_GAIN))

    def setFocusSlot(self):
        value = self.focus.slider.value() * 5
        setCVParam(cv2.CAP_PROP_FOCUS, value)

    def setExposureSlot(self):
        value = self.exposure.slider.value()
        setCVParam(cv2.CAP_PROP_EXPOSURE, value)

    def setBrightnessSlot(self):
        value = self.brightness.slider.value()
        setCVParam(cv2.CAP_PROP_BRIGHTNESS, value)

    def setContrastSlot(self):
        value = self.contrast.slider.value()
        setCVParam(cv2.CAP_PROP_CONTRAST, value)

    def setSaturationSlot(self):
        value = self.saturation.slider.value()
        setCVParam(cv2.CAP_PROP_SATURATION, value)

    def setHueSlot(self):
        value = self.hue.slider.value()
        setCVParam(cv2.CAP_PROP_HUE, value)

    def setGainSlot(self):
        value = self.gain.slider.value()
        setCVParam(cv2.CAP_PROP_GAIN, value)


class CameraCapture(QWidget):
    changePixmap = pyqtSignal(QImage)

    def __init__(self):
        super().__init__()
        self.label = QLabel(self)

        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        layout.addWidget(self.label)
        layout.setSizeConstraint(layout.SetNoConstraint)
        self.setLayout(layout)

        th = Thread(self)
        th.changePixmap.connect(self.setImage)
        th.start()

    @pyqtSlot(QImage)
    def setImage(self, image):
        self.current = image

        width = self.label.width()
        height = self.label.height()
        self.label.setPixmap(QPixmap.fromImage(image).scaled(width, height, Qt.KeepAspectRatio))

        self.changePixmap.emit(image)


class ImageStrip(QWidget):
    def __init__(self):
        super().__init__()
        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(2, 2, 2, 2)
        self.layout.setSpacing(2)
        self.layout.setSizeConstraint(self.layout.SetNoConstraint)
        self.layout.addStretch()
        self.setLayout(self.layout)

    def addImage(self, filename):
        width, height = 192, 100
        image = QImage(filename)
        label = QLabel(self)
        label.setScaledContents(True)
        label.setPixmap(QPixmap.fromImage(image).scaled(width, height, Qt.KeepAspectRatio))
        label.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.layout.insertWidget(self.layout.count() - 1, label)


class MainApp(QWidget):
    def __init__(self):
        super().__init__()
        layout = QHBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        layout.setSpacing(5)
        layout.setSizeConstraint(layout.SetNoConstraint)
        self.setLayout(layout)

        self.camera_capture = CameraCapture()
        self.image_strip = ImageStrip()
        self.scroll_area = QScrollArea(self)
        self.scroll_area.setFixedHeight(130)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(self.image_strip)

        camera_layout = QVBoxLayout()
        camera_layout.addWidget(self.camera_capture)
        camera_layout.addWidget(self.scroll_area)

        self.camera_capture.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum))
        self.controls = Controls()
        self.controls.setFixedWidth(500)

        layout.addWidget(self.controls)
        layout.addLayout(camera_layout)

        self.frame = 0
        self.current = None

        self.camera_capture.changePixmap.connect(self.changeImage)

    @pyqtSlot(QImage)
    def changeImage(self, image):
        self.current = image.copy()

    def saveFrame(self, frame):
        filename = self.controls.outputPath.getOutputPath()
        filename = filename.format(frame=frame)
        print(filename)
        self.current.save(filename, 'PNG', quality=100)
        return filename

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            frame = self.controls.outputPath.getFrame()
            filename = self.saveFrame(frame)
            self.image_strip.addImage(filename=filename)
            self.controls.outputPath.setFrame(frame + 1)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainApp()
    ex.resize(1550, 600)
    ex.show()
    sys.exit(app.exec_())
